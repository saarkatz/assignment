
# For python 2.7
import argparse
import subprocess
import os
import sys
import datetime

# Handle argument
parser = argparse.ArgumentParser(description='Runs the specified dockerfiles and prints which were successfull.')
parser.add_argument('paths', metavar='PATH', nargs='+',
			help='Paths of the dockerfiles.')
parser.add_argument('-v', action='store_true',
			help='If present, the actions of the docker will be printed to the terminal')
parser.add_argument('-k', '--keep', action='store_true',
			help="If present, the created images won't be deleted at the end of execution")
args = parser.parse_args()

# A little color to spice up the terminal.
red = '\033[91m'
green = '\033[92m'
end = '\033[0m'

# Apply -v flag.
if not args.v:
	stdout_s=open(os.devnull, 'wb')
	stderr_s=open(os.devnull, 'wb')
else:
	stdout_s=sys.stdout
	stderr_s=sys.stderr

# Iterate over the dockerfiles.
generated_images = []
duplicate = 0
for path in args.paths:
	# Generate a unique tag for the image
	tag = 'image_' + datetime.datetime.now().strftime("%y%m%d_%H%M%S") + "_" + str(duplicate)
	while tag in generated_images:
		duplicate += 1
		tag = 'image_' + datetime.datetime.now().strftime("%y%m%d_%H%M%S") + "_" + str(duplicate)
	else:
		duplicate = 0
	# Run the dockerfile
	print "Dockerfile at: " + os.path.join(path, 'Dockerfile') + ' with the name: "' + tag + '"'
	result = subprocess.call(["docker", "build", "-t", tag, path], stdout=stdout_s, stderr=stderr_s)
	if result == 0:
		print " - "+green+"Success"+end
		generated_images.append(tag)
	else:
		print " - "+red+"Fail"+end+" with return code: "+str(result)

# Apply -k flag
if not args.keep:
	subprocess.call(["docker", "rmi"] + generated_images, stdout=stdout_s, stderr=stderr_s)
